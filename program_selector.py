import subprocess
import os
import json

# _notepad = "C:\WINDOWS\system32/notepad.exe"
# _firefox = "C://Program Files/Mozilla Firefox/firefox.exe"

# _codeWars = "https://www.codewars.com"
# _codeAcademy = "https://www.codeacademy.com"

# _pycharm = "C:\\Program Files\\JetBrains\\PyCharm Community Edition 2018.1.1\\bin\pycharm64.exe"
# _vsCode = "C:\\Program Files\\Microsoft VS Code\Code.exe"
# _visualStudio = "C:\Program Files (x86)\Microsoft Visual Studio\\2017\Enterprise\Common7\IDE\devenv.exe"


_pyNames = ["py", "python", "pycharm", "p"]
_vsCodeNames = ["vscode", "react", "vs", "code"]
_studioNames = ["vstudio", "studio", "c", "v"]


# json
_IDEJson = {
    "data":
    [{
        "programName": "python",
        "names": ["py", "python", "pycharm"],
        "codePath": "C:\WINDOWS\system32/notepad.exe"
    },
        {
        "programName": "vsCode",
        "names": ["vs", "code", "vscode", "react"],
        "codePath": "C:\\Program Files\\Microsoft VS Code\\Code.exe"
    },
        {
        "programName": "vsStudio",
        "names": ["visual", "studio", ".net", "c#"],
        "codePath": "C:\\Program Files (x86)\\Microsoft Visual Studio\\2017\\Enterprise\\Common7\\IDE\\devenv.exe"
    }]
}

# Fla Loop
# for data in _IDEJson["data"]:
#     print(data)


def questionIDE():
    _question = input("Name an IDE: \n - pyCharm \n - vsCode \n - visual Studio")
    _question = _question.lower()

    for data in _IDEJson["data"]:
        for name in data["names"]:
            print(name)
            if _question == name and data["programName"] == "python":
                print("--Doing python")
                os.startfile(data["codePath"])
                break
            elif _question == name and data["programName"] == "vsCode":
                print("--Doing vsCode")
                os.startfile(data["codePath"])
                break
            elif _question == name and data["programName"] == "vsStudio":
                print("--Doing vsStudio Code")
                os.startfile(data["codePath"])
                break

questionIDE()

#     for pName in data["programName"] == _question:
#         print("\n Question answer was: ", _question)
#         print("Available Names to use: ", pName)

# for name in data["names"]:
#     if :
#         print('--------------------------------')
#         print("Printing Name:", name)
#         os.startfile(_IDEJson["data"][0]["codePath"])


# for k, v in _IDENames.items():
#     if _question == _IDENames[v]:
#         os.startfile(_notepad)
#         print("entering pyNames")
#     elif _question == _IDENames["_vsCodeNames"]:
#         os.startfile(_vsCode)
#         print("entering vsCode")
#     else:
#         print("Nothing happened")

# print(_IDENames["_pyNames"])

# for names in _IDENames:
#     if _question == _IDENames[names]:
#         os.startfile(_notepad)
#
# if _IDE == py_names():
#     os.startfile(_notepad)
# elif _IDENames == vscode_names():
#     os.startfile(_vsCode)
# if _IDE == "py" or "pycharm" or "python" or "p":
#     # os.startfile(_pycharm)
#     os.startfile(_notepad)
#     # subprocess.call([_firefox, _codeWars, "http://google.com"])
# elif _IDE == "vscode":
#     os.startfile(_vsCode)
# elif _IDE == "studio" or "vs":
#     os.startfile(_visualStudio)
#
# for x in names:
#     if _IDE == x:
#         os.startfile(_visualStudio)
#


# {-
#     key1: value1,
#     key2: [{},{}],
#     key3: 1,
#     key5: true,
#     key6: [1,3,4]
# }
