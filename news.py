import os
import subprocess
import webbrowser

_bbcNews = 'https://www.bbc.co.uk/news'
_worldNews = 'https://www.reddit.com/r/worldNews'

#programs to run
_firefox = 'C://Program Files/Mozilla Firefox/firefox.exe'
_chrome = 'C:/Program Files (x86)/Google/Chrome/Application/chrome.exe'

question = input('Do you want to open Chrome too? ')

if(question == 'y'):
    os.startfile(_chrome)
else:
    print('Chrome not opened')

subprocess.call([_firefox, _bbcNews, _worldNews])